using UnityEngine;

namespace General
{
    public class CoroutineService : MonoBehaviour
    {
        private static CoroutineService _serviceInstance;
        
        public static CoroutineService Instance
        {
            get
            {
                if (_serviceInstance == null)
                {
                    _serviceInstance = new GameObject("CoroutineService").AddComponent<CoroutineService>();

                    DontDestroyOnLoad(_serviceInstance);
                }

                return _serviceInstance;
            }
        }

        
    }
}