using System;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Tasks
{
    public class TasksExample : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _text;
        [SerializeField] [Range(0, 5)]
        private float _delay;
        [SerializeField]
        private string _labelString;
        [SerializeField]
        private Color _labelColor;

        [ContextMenu("Change Text")]
        public async void ChangeText()
        {
            try
            {
                var textData = _labelString;
                
                await new WaitForSeconds(_delay);
                
                _text.SetText(textData);
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
        }

        [ContextMenu("Handled exception")]
        public async void HandledException()
        {
            try
            {
                await ThrowExceptionAsync();
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
        }

        [ContextMenu("Unhandled Exception")]
        public void UnhandledException()
        {
            ThrowExceptionAsync();
        }

        [ContextMenu("Branches")]
        public void Branches()
        {
            SetTextAsync().WrapErrors();
            SetColorAsync().WrapErrors();
        }

        [ContextMenu("When Any")]
        public async void WhenAny()
        {
            try
            {
                var monoCancellationToken = this.GetCancellationTokenOnDestroy();

                using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(monoCancellationToken))
                {
                    var cancellationToken = linkedCts.Token;
                    var textTask = SetTextAsync(cancellationToken);
                    var colorTask = SetColorAsync(cancellationToken);
                    var firstTask = await Task.WhenAny(textTask, colorTask);
                    var firstTaskString = firstTask == textTask ? "TEXT" : "COLOR";
                    
                    linkedCts.Cancel();
                    
                    await firstTask;
                    
                    Debug.LogWarning($"First task to complete is {firstTaskString}");
                }
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
        }

        [ContextMenu("When All")]
        public async void WhenAll()
        {
            try
            {
                var cancellationToken = this.GetCancellationTokenOnDestroy();
                
                var textTask = SetTextAsync(cancellationToken);
                var colorTask = SetColorAsync(cancellationToken);
                await Task.WhenAll(textTask, colorTask);

                Debug.LogWarning("All tasks have completed");
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
            }
        }

        private async Task SetTextAsync(CancellationToken cancellationToken = default)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_delay), cancellationToken: cancellationToken);

            _text.SetText(_labelString);
            
            //throw new Exception("More exceptions!");
        }

        private async Task SetColorAsync(CancellationToken cancellationToken = default)
        {
            await Task.Delay(TimeSpan.FromSeconds(_delay * 2), cancellationToken);

            _text.color = _labelColor;
        }

        private async Task ThrowExceptionAsync()
        {
            await Awaiters.Seconds(_delay);
            
            throw new Exception("This is a very sneaky exception");
        }
    }
}