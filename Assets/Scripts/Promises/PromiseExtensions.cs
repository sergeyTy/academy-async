using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using RSG;

namespace Promises
{
    public static class PromiseExtensions
    {
        public static Task AsTask(this Promise promise, CancellationToken cancellationToken = default)
        {
            var tcs = new TaskCompletionSource<bool>();
            cancellationToken.Register(() =>
            {
                promise.Reject(new OperationCanceledException());
            });

            promise
                .Then(() => tcs.TrySetResult(true))
                .Catch(exception => tcs.TrySetException(exception))
                .Done();

            return tcs.Task;
        }

        public static TaskAwaiter GetAwaiter(this Promise promise)
        {
            return promise.AsTask().GetAwaiter();
        }

        public static void SafeResolve(this Promise promise)
        {
            if (promise.CurState == PromiseState.Pending)
            {
                promise.Resolve();
            }
        }
    }
}