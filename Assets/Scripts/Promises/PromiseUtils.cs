using System;
using System.Collections;
using General;
using RSG;
using UnityEngine;

namespace Promises
{
    public static class PromiseUtils
    {
        public static Promise PromiseCoroutine(IEnumerator enumerator)
        {
            var promise = new Promise();

            CoroutineService.Instance.StartCoroutine(PromiseWrapCoroutine(enumerator, promise));

            return promise;
        }
        
        public static Promise PromiseCoroutine(YieldInstruction yieldInstruction)
        {
            var promise = new Promise();

            CoroutineService.Instance.StartCoroutine(PromiseWrapCoroutine(yieldInstruction, promise));

            return promise;
        }

        private static IEnumerator PromiseWrapCoroutine(IEnumerator enumerator, IPendingPromise pendingPromise)
        {
            yield return enumerator;
            
            pendingPromise.Resolve();
        }
        
        private static IEnumerator PromiseWrapCoroutine(YieldInstruction yieldInstruction, Promise pendingPromise)
        {
            pendingPromise.ReportProgress(0.5f);
            yield return yieldInstruction;
            pendingPromise.ReportProgress(1.0f);
            
            pendingPromise.Resolve();
        }
    }
}