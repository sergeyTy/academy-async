using System;
using RSG;
using TMPro;
using UnityEngine;

namespace Promises
{
    public class PromiseExample : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _text;
        [SerializeField] [Range(0, 5)]
        private float _delay;
        [SerializeField]
        private string _labelString;
        [SerializeField]
        private Color _labelColor;

        private void Start()
        {
            Promise.UnhandledException += OnPromiseUnhandledException;
        }

        private void OnDestroy()
        {
            Promise.UnhandledException -= OnPromiseUnhandledException;
        }

        [ContextMenu("Change Text")]
        public void ChangeText()
        {
            WaitFor(_delay)
                .Progress(progress => Debug.LogWarning($"Progress is {progress}"))
                .Then(() => Promise<string>.Resolved(_labelString))
                .Then(receivedString => _text.SetText(receivedString))
                .Catch(Debug.LogException)
                .Done();
        }

        [ContextMenu("Handled exception")]
        public void HandledException()
        {
            WaitFor(_delay)
                .Then(() => throw new Exception("Intentional whoopsie UwU"))
                .Catch(exception => Debug.LogError($"Handled exception with message \"{exception.Message}\""))
                .Done();
        }

        [ContextMenu("Unhandled Exception")]
        public void UnhandledException()
        {
            WaitFor(_delay)
                .Then(() => throw new Exception("Unintentional whoopsie OwO"))
                .Finally(() => Debug.LogError("Oh look, we can still do something!"))
                .Done();
        }

        [ContextMenu("Rejected promise")]
        public void RejectedPromise()
        {
            Promise.Rejected(new Exception("Oh nooooo, I got rejected ;_;"))
                .Then(() => _text.SetText("You'll never see this, muhahaha!"))
                .Catch(Debug.LogError)
                .Done();
        }

        [ContextMenu("Promise branches")]
        public void PromiseBranches()
        {
            var targetColor = Color.green;
            var initialPromise = WaitFor(_delay)
                .Then(() => targetColor = _labelColor)
                .Then(() => throw new Exception("Looks like an error"))
                .Then(() => targetColor = Color.red)
                .Finally(() => Debug.LogWarning("Idk, do something"))
                .Catch(Debug.LogException)
                .Then(() => Promise<string>.Resolved(_labelString));
            
            initialPromise
                .Then(textData => _text.SetText(textData))
                .Catch(Debug.LogException)
                .Done();
            
            initialPromise
                .Then(_ => WaitFor(_delay))
                .Then(() => _text.color = targetColor)
                .Catch(Debug.LogException)
                .Done();
        }

        [ContextMenu("Race Promise")]
        public void RacePromise()
        {
            var promise1 = WaitFor(_delay)
                .Then(() => throw new Exception("Racer got shot in the knee!"))
                .Then(() => Promise<int>.Resolved(1));
            var promise2 = WaitFor(_delay * 0.5f)
                .Then(() => Promise<int>.Resolved(2));

            Promise<int>.Race(promise1, promise2)
                .Then(racerNumber => Debug.LogWarning($"Racer with number {racerNumber} won!"))
                .Catch(Debug.LogException)
                .Done();
        }

        [ContextMenu("All Promise")]
        public void AllPromise()
        {
            var promise1 = WaitFor(_delay)
                .Then(() => throw new Exception("Everybody panic!"))
                .Then(() => Promise<int>.Resolved(1));
            var promise2 = WaitFor(_delay * 0.5f)
                .Then(() => Promise<int>.Resolved(2));
            
            Promise<int>.All(promise1, promise2)
                .Then(results =>
                {
                    var resultString = "Resulting numbers: ";

                    foreach (var result in results)
                    {
                        resultString += $" {result}";
                    }
                    
                    Debug.LogWarning(resultString);
                })
                .Catch(Debug.LogException)
                .Done();
        }

        private IPromise WaitFor(float duration)
        {
            return PromiseUtils.PromiseCoroutine(new WaitForSeconds(duration));
        }

        private void OnPromiseUnhandledException(object obj, ExceptionEventArgs exceptionEventArgs)
        {
            Debug.LogError($"Unhandled promise exception with message \"{exceptionEventArgs.Exception.Message}\"");
        }
    }
}